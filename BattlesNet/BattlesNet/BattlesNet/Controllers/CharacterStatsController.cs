﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using BattlesNet.Data;
using BattlesNet.Models;

namespace BattlesNet.Controllers
{
    public class CharacterStatsController : ApiController
    {
        private BattlesNetContext db = new BattlesNetContext();

        // GET: api/CharacterStats
        public IQueryable<CharacterStat> GetCharacterStats()
        {
            return db.CharacterStats;
        }

        // GET: api/CharacterStats/5
        [ResponseType(typeof(CharacterStat))]
        public IHttpActionResult GetCharacterStat(int id)
        {
            CharacterStat characterStat = db.CharacterStats.Find(id);
            if (characterStat == null)
            {
                return NotFound();
            }

            return Ok(characterStat);
        }

        // PUT: api/CharacterStats/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutCharacterStat(int id, CharacterStat characterStat)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != characterStat.Id)
            {
                return BadRequest();
            }

            db.Entry(characterStat).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CharacterStatExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/CharacterStats
        [ResponseType(typeof(CharacterStat))]
        public IHttpActionResult PostCharacterStat(CharacterStat characterStat)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.CharacterStats.Add(characterStat);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = characterStat.Id }, characterStat);
        }

        // DELETE: api/CharacterStats/5
        [ResponseType(typeof(CharacterStat))]
        public IHttpActionResult DeleteCharacterStat(int id)
        {
            CharacterStat characterStat = db.CharacterStats.Find(id);
            if (characterStat == null)
            {
                return NotFound();
            }

            db.CharacterStats.Remove(characterStat);
            db.SaveChanges();

            return Ok(characterStat);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool CharacterStatExists(int id)
        {
            return db.CharacterStats.Count(e => e.Id == id) > 0;
        }
    }
}
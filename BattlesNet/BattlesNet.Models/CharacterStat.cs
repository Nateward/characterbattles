﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BattlesNet.Models
{
    public class CharacterStat
    {
        public int Id { get; set; }
        public int StatId { get; set; }
        public int CharacterId { get; set; }
        public int Value { get; set; }

    }
}
﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace BattlesNet.Models
{
    public class Battle
    {
        public int Id { get; set; }
        public int CharacterId { get; set; }
        public int EnemyId { get; set; }
        public bool Win { get; set; }
        public UInt16 Exp { get; set; }
        public DateTime Date { get; set; }

        public virtual Character Character { get; set; }
        public virtual Character Enemy { get; set; }

    }
}
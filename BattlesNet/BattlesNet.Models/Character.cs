﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BattlesNet.Models
{
    public class Character
    {
        public int Id { get; set; }
        public int RaceId { get; set; }
        public byte Age { get; set; }
        public string Name { get; set; }
        [MaxLength(255,ErrorMessage ="Can not enter a description longer than 255 characters" )]
        public string Desc { get; set; }
        public virtual Race Race { get; set; }
        public virtual List<CharacterStat> CharacterStats { get; set; }
        public virtual List<Battle> Battles { get; set; }
        public virtual List<Battle> EmenyBattles { get; set; }
    }

   
}
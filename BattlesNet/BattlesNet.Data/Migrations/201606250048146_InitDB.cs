namespace BattlesNet.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitDB : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Battles",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CharacterId = c.Int(nullable: false),
                        EnemyId = c.Int(nullable: false),
                        Win = c.Boolean(nullable: false),
                        Date = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Characters", t => t.CharacterId)
                .ForeignKey("dbo.Characters", t => t.EnemyId)
                .Index(t => t.CharacterId)
                .Index(t => t.EnemyId);
            
            CreateTable(
                "dbo.Characters",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        RaceId = c.Int(nullable: false),
                        Age = c.Byte(nullable: false),
                        Name = c.String(),
                        Desc = c.String(maxLength: 255),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Races", t => t.RaceId, cascadeDelete: true)
                .Index(t => t.RaceId);
            
            CreateTable(
                "dbo.CharacterStats",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        StatId = c.Int(nullable: false),
                        CharacterId = c.Int(nullable: false),
                        Value = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Characters", t => t.CharacterId, cascadeDelete: true)
                .Index(t => t.CharacterId);
            
            CreateTable(
                "dbo.Races",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Stats",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Battles", "EnemyId", "dbo.Characters");
            DropForeignKey("dbo.Battles", "CharacterId", "dbo.Characters");
            DropForeignKey("dbo.Characters", "RaceId", "dbo.Races");
            DropForeignKey("dbo.CharacterStats", "CharacterId", "dbo.Characters");
            DropIndex("dbo.CharacterStats", new[] { "CharacterId" });
            DropIndex("dbo.Characters", new[] { "RaceId" });
            DropIndex("dbo.Battles", new[] { "EnemyId" });
            DropIndex("dbo.Battles", new[] { "CharacterId" });
            DropTable("dbo.Stats");
            DropTable("dbo.Races");
            DropTable("dbo.CharacterStats");
            DropTable("dbo.Characters");
            DropTable("dbo.Battles");
        }
    }
}

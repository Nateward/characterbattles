function getParameterByName(name, url) {
  if (!url) url = window.location.href;
  name = name.replace(/[\[\]]/g, "\\$&");
  var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
    results = regex.exec(url);
  if (!results) return null;
  if (!results[2]) return '';
  return decodeURIComponent(results[2].replace(/\+/g, " "));
}

function loadCharacterData(callback, id) {
  $.ajax({
    method: 'GET',
    url: 'characters.json',
    dataType: 'JSON',
    success: function(data) {
      callback(data, id);
    }

  });
}

function fillEdit(characters, id) {
  var tempCharacter = {};
  $.each(characters, function(index, character) {
    if (character.Id == id) {
      tempCharacter = character;
    }
  });
  $('#characterId').val(tempCharacter.Id);
  $('#name').val(tempCharacter.Name);
  $('#age').val(tempCharacter.Age);
  $('#race').val(tempCharacter.Race);
  $.each(tempCharacter.Stats, function(index, stat){
    var row = "<tr><td>"+stat.Name+"</td><td>"+stat.Value+"</td><td style='display: none'><div class='btn btn-sm btn-success pull-left'><div class='glyphicon glyphicon-plus-sign'></div></div></td></tr>"
   console.log(".here")
    $('#stats').append(row);
  });
  
}

$(document).ready(function() {
  var characterId = getParameterByName('CharacterID');
  loadCharacterData(fillEdit, characterId);


});